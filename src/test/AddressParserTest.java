package test;

import main.Address;
import main.AddressParser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

class AddressParserTest {
    private static final String TEST_LINE = "12345, TestCity, TestStreet, 12";

    @Test
    void checkAllAsExpected() throws IOException {
        List<String> lines = new ArrayList<>();
        lines.add(TEST_LINE);
        List<Address> addresses = AddressParser.parseAddresses(lines);
        Assertions.assertEquals(addresses.get(0).getIndex(), 12345, "Index is not as expected!");
        Assertions.assertEquals(addresses.get(0).getCity(), "TestCity", "City is not as expected!");
        Assertions.assertEquals(addresses.get(0).getStreet(), "TestStreet", "Street is not as expected!");
        Assertions.assertEquals(addresses.get(0).getNumber(), 12, "Number is not as expected!");
    }
}