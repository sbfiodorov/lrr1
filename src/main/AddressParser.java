package main;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class AddressParser {
    public static List<Address> parseAddresses(List<String> lines) {
        ArrayList<Address> addresses = new ArrayList<>();
        for (String line : lines) {
            String[] fragments = line.split(", ");
            addresses.add(new Address(Integer.parseInt(fragments[0]), fragments[1], fragments[2], Integer.parseInt(fragments[3])));
        }
        return addresses;
    }

    public static Address findMaxIndex(List<Address> addresses) {
        return addresses.stream()
                .max(Comparator.comparing(Address::getIndex)).orElse(null);
    }

    public static Address findMinIndex(List<Address> addresses) {
        return addresses.stream()
                .min(Comparator.comparing(Address::getIndex)).orElse(null);
    }

    public static Address findMaxStreetLength(List<Address> addresses) {
        return addresses.stream()
                .max(Comparator.comparing(address1 -> address1.getStreet().length())).orElse(null);
    }

    public static Address findMinStreetLength(List<Address> addresses) {
        return addresses.stream()
                .min(Comparator.comparing(address1 -> address1.getStreet().length())).orElse(null);
    }
}