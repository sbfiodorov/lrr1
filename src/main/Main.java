package main;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Main {
    public static String file = "data/input.txt";

    public static void main(String[] args) throws IOException {
        List<String> lines = Files.readAllLines(Paths.get(file));
        List<Address> addresses = AddressParser.parseAddresses(lines);
        System.out.println("\n");
        addresses.forEach(System.out::println);

        System.out.println("\n");
        System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        System.out.println("\n");

        /*Минимальный и максимальный индексы и длинны строк улиц*/
        Address minIndex = AddressParser.findMinIndex(addresses);
        System.out.println(minIndex);
        Address maxIndex = AddressParser.findMaxIndex(addresses);
        System.out.println(maxIndex);
        Address minStreet = AddressParser.findMinStreetLength(addresses);
        System.out.println(minStreet);
        Address maxStreet = AddressParser.findMaxStreetLength(addresses);
        System.out.println(maxStreet);
    }
}