package main;

public class Address {
    private Integer index;
    private String city;
    private String street;
    private Integer number;

    public Address(Integer index, String city, String street, Integer number) {
        this.city = city;
        this.street = street;
        this.number = number;
        this.index = index;
    }

    public Integer getIndex() {
        return index;
    }

    public String getCity() {
        return city;
    }

    public String getStreet() {
        return street;
    }

    public Integer getNumber() {
        return number;
    }

    public String toString() {
        return "Индекс - " + index + ", Название города - " + city + ", Название улицы - " + street + ", Номер дома - " + number;
    }
}